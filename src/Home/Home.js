import React, {useState} from 'react';
import {Form, Table} from "react-bootstrap";
import AppContainer from "../hoc/AppContainer";
import './Home.css'
import {Button} from '@mui/material';
import 'bootstrap/dist/css/bootstrap.min.css';

const Home = () => {
    //localStorage.clear()
    const [ pay, setPay] = useState(0)
    const heading = ['#', 'Navn'];
    const password = 'mozart';
    let body = [];
    let local = localStorage.getItem('table');
    let perLodd = localStorage.getItem('pris');
    if (perLodd === null){
        perLodd = 10;
    }

    if (local === null){
        for (let i = 1; i <= 100; i++){
            body.push([i, '']);
        }
    }
    else {
        body = JSON.parse(local).bodyRow
    }

    const [ lodd, setLodd] = useState({
        bodyRow: body,
    })

    const onInputChange = (event, nr) => {
        const newBod = lodd.bodyRow.slice()
        if (event.target.value.length===0){
            setPay(pay => pay - 1)
        }
        else if (event.target.value.length===1 && event.nativeEvent.data){
            setPay(pay => pay + 1)
        }
        else if (newBod[nr[0]-1][1].length < 1){
            setPay(pay => pay + 1)
        }
        newBod[nr[0]-1][1] = event.target.value;
        setLodd({
            ...lodd,
            [event.target.id]: newBod
        })
    }

    const onFormSubmit = event => {
        event.preventDefault()
        window.confirm("Er du sikker på at du vil kjøpe "+pay+" lodd for "+pay*perLodd+" kr?\n\n" +
            "Vipps beløp til 92046664 og klikk OK.");
        body=lodd
        localStorage.setItem('table', JSON.stringify(body));
        window.location.reload();
    }

    const tilfeldig = event => {
        event.preventDefault()
        let ant = prompt("Hvor mange lodd vil du kjøpe?");
        if(isNaN(ant)){
            alert("Du må skrive inn tall!")
        }
        else {
            let nm = prompt("Hvilket navn skal skrives opp?");
            window.confirm("Er du sikker på at du vil kjøpe "+ant+" lodd for "+ant*perLodd+" kr?\n\n" +
                "Vipps beløp til 92046664 og klikk OK.");
            let rand;
            for (let i = 0; i < ant; i++){
                while (true){
                    rand = Math.round(Math.random() * (100 - 1) + 1)-1;
                    if(body[rand][1].length < 1){
                        body[rand][1] = nm;
                        break;
                    }
                }
            }
            setLodd({
                ...lodd,
                [event.target.id]: body
            })
            body=lodd;
            localStorage.setItem('table', JSON.stringify(body));
            window.location.reload();
        }
    }

    const newTable = event => {
        event.preventDefault()
        let psw = prompt("Skriv passord for å starte et nytt lotteri");
        if (psw != null){
            if (psw.toLowerCase() !== password) {
                alert("Feil passord");
            }
            else {
                let ant = prompt("Hvor mange vin ble kjøpt inn?");
                let pris = prompt("Hvor mye kostet det?");
                let prisPer = parseInt(pris)/100;
                if(isNaN(prisPer)){
                    alert("Du må skrive inn tall!")
                }
                else {
                    localStorage.clear();
                    localStorage.setItem('ant', ant);
                    localStorage.setItem('pris', String(Math.round(prisPer)));
                    window.location.reload();
                }
            }
        }
    }
    const trekkLodd = event => {
        event.preventDefault()
        let psw = prompt("Skriv passord for å starte trekningen");
        if (psw != null){
            if (psw.toLowerCase() !== password) {
                alert("Feil passord");
            }
            else {
                let ant = localStorage.getItem('ant');
                let winnerNum = 0;
                let winner = '';
                let winnerlist = []
                for (let i = ant; i>0; i--){
                    while (winner.length<=0){
                        winnerNum = Math.round(Math.random() * (100 - 1) + 1);
                        winner = body[winnerNum-1][1];
                        if (winner.length > 0){
                            for (let w in winnerlist){
                                if (winnerlist[w] === winnerNum){
                                    winner= ''
                                }
                            }
                            if (winner.length > 0){
                                winnerlist.push(winnerNum)
                            }
                        }
                    }
                    alert("Vinneren er "+winner+" med tallet "+winnerNum+"!\n\nViner igjen: "+(i-1));
                    winner = '';
                }
            }
        }
    }


    return (
            <main>
                <AppContainer>
                    <div className="header">
                        <h3>Experis vinlotteri</h3>
                        <img src="https://cdn.picpng.com/wine/pattern-wine-27878.png" alt=""/>
                    </div>
                    <Form onSubmit={ onFormSubmit }>
                    <div className="pay">
                        <p>Pris per lodd: {perLodd} kr</p>
                        <p>Total pris: {pay*perLodd} kr</p>
                        <Button sx={{margin: 1}} color="secondary" variant="contained" type="submit">Kjøp lodd</Button>
                        <Button sx={{margin: 1}} color="secondary" variant="contained" onClick={tilfeldig}>Tilfeldig tall</Button>
                        <Button sx={{margin: 1}} color="primary" variant="contained" onClick={trekkLodd}>Trekk lodd</Button>
                        <Button sx={{margin: 1}} color="primary" variant="contained" onClick={newTable}>Nytt lotteri</Button>
                    </div>
                        <div className="tables">
                        <Table striped bordered hover variant="dark" sx={{width: 30}}>
                            <thead>
                            <tr>
                                {heading.map(head => <th key={head}>{head}</th>)}
                            </tr>
                            </thead>
                            <tbody>
                            {body.map(row =>
                                <tr key={row}>
                                    {row[0] <= 50 &&
                                    row.map(val =>
                                        <td key={val}>
                                            {val > 0 &&
                                            <h5 className="number">
                                                {val}
                                            </h5>}
                                            {val.length > 0 &&
                                            <h5 className="name">
                                                {val}
                                            </h5>}
                                            {val.length <= 0 &&
                                            <input type="text" onChange={(e) => onInputChange(e, row)}/>}
                                        </td>)}
                                </tr>)}
                            </tbody>
                        </Table>
                        <Table striped bordered hover variant="dark">
                            <thead>
                            <tr>
                                {heading.map(head => <th key={head}>{head}</th>)}
                            </tr>
                            </thead>
                            <tbody>
                            {body.map(row =>
                                <tr key={row}>
                                    {row[0] > 50 &&
                                    row.map(val =>
                                        <td key={val}>
                                            {val > 0 &&
                                            <h5 className="number">
                                                {val}
                                            </h5>
                                            }
                                            {val.length > 0 &&
                                            <h5 className="name">
                                                {val}
                                            </h5>
                                            }
                                            {val.length <= 0 &&
                                            <input type="text" onChange={(e) => onInputChange(e, row)}/>
                                            }
                                        </td>)
                                    }
                                </tr>)}
                            </tbody>
                        </Table>

                        </div>
                    </Form>
                </AppContainer>
            </main>
        );
}

export default Home;