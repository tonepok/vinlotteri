import React, {useState} from 'react';
import {Form} from "react-bootstrap";
import AppContainer from "../hoc/AppContainer";
import './Home.css'
import {Button} from '@mui/material';

const Home = () => {
    const [ pay, setPay] = useState(0)
    const perLodd = 10;
    const heading = ['Nummer', 'Navn'];
    let body = [];
    let local = localStorage.getItem('table');

    if (local === null){
        body.push([1, 'Tone']);
        for (let i = 2; i <= 100; i++){
            body.push([i, '']);
        }
    }
    else {
        body = JSON.parse(local).bodyRow
    }

    const [ lodd, setLodd] = useState({
        bodyRow: body,
    })

    const onInputChange = (event, nr) => {
        if (event.target.value.length===0){
            setPay(pay => pay - 1)
        }
        else if (event.target.value.length===1 && event.nativeEvent.data){
            setPay(pay => pay + 1)
        }
        const newBod = lodd.bodyRow.slice()
        newBod[nr[0]-1][1] = event.target.value;
        setLodd({
            ...lodd,
            [event.target.id]: newBod
        })
    }

    const onFormSubmit = event => {
        event.preventDefault()
        window.confirm("Er du sikker på at du vil kjøpe "+pay+" lodd for "+pay*perLodd+" kr?\n\n" +
            "Vipps beløp til 92046664 og klikk OK.");
        body=lodd
        localStorage.setItem('table', JSON.stringify(body));
        //console.log(localStorage.getItem('table'))
        window.location.reload();
    }

    const newTable = event => {
        event.preventDefault()
        let psw = prompt("Skriv passord for å starte et nytt lotteri");
        if (psw !== "Mozart") {
            alert("Feil passord");
        }
        else {
            localStorage.clear();
            window.location.reload();
        }
    }

    return (
        <main>
            <AppContainer>
                <div className="header">
                    <h3>Experis vinlotteri</h3>
                    <img src="https://cdn.picpng.com/wine/pattern-wine-27878.png" alt=""/>
                </div>
                <Form onSubmit={ onFormSubmit }>
                    <div className="pay">
                        <p>Pris per lodd: {perLodd} kr</p>
                        <p>Total pris: {pay*perLodd} kr</p>
                        <Button color="secondary" variant="contained" type="submit">Kjøp lodd</Button>
                        <Button sx={{margin: 1}} color="primary" variant="contained" onClick={newTable}>Nytt lotteri</Button>
                    </div>
                    <div className="tables">
                        <table>
                            <thead>
                            <tr>
                                {heading.map(head => <th>{head}</th>)}
                            </tr>
                            </thead>
                            <tbody>
                            {body.map(row =>
                                <tr>
                                    {row[0] <= 50 &&
                                    row.map(val =>
                                        <td>
                                            {val > 0 &&
                                            <h5 className="number">
                                                {val}
                                            </h5>}
                                            {val.length > 0 &&
                                            <h5 className="name">
                                                {val}
                                            </h5>}
                                            {val.length <= 0 &&
                                            <input type="text" onChange={(e) => onInputChange(e, row)}/>}
                                        </td>)}
                                </tr>)}
                            </tbody>
                        </table>
                        <table>
                            <thead>
                            <tr>
                                {heading.map(head => <th>{head}</th>)}
                            </tr>
                            </thead>
                            <tbody>
                            {body.map(row =>
                                <tr>
                                    {row[0] > 50 &&
                                    row.map(val =>
                                        <td>
                                            {val > 0 &&
                                            <h5 className="number">
                                                {val}
                                            </h5>
                                            }
                                            {val.length > 0 &&
                                            <h5 className="name">
                                                {val}
                                            </h5>
                                            }
                                            {val.length <= 0 &&
                                            <input type="text" onChange={(e) => onInputChange(e, row)}/>
                                            }
                                        </td>)
                                    }
                                </tr>)}
                            </tbody>
                        </table>

                    </div>
                </Form>
            </AppContainer>
        </main>
    );
}

export default Home;